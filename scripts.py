#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random, time, traceback

# TECH INSTABOT
def techInstabotNew(InstaPy, username, password, proxy):
    try:
        session = InstaPy(username=username, password=password, headless_browser=False)
        session.login()

        """ Start of parameter setting """
        # don't like if a post already has more than 150 likes
        session.set_delimit_liking(enabled=True, max_likes=500, min_likes=None)

        # set action delays
        session.set_action_delays(enabled=True,
            like=75,
            comment=45,
            follow=78,
            unfollow=56,
            randomize=True,
            random_range_from=150,
            random_range_to=300)

        """I used to have potency_ratio=-0.85 and max_followers=1200 for set_relationship_bounds()
            Having a stricter relationship bound to target only low profiles users was not very useful,
            as interactions/sever calls ratio was very low. I would reach the server call threshold for
            the day before even crossing half of the presumed safe limits for likes, follow and comments (yes,
            looks like quiet a lot of big(bot) managed accounts out there!!).
            So I relaxed it a bit to -0.50 and 2000 respectively.
        """
        session.set_relationship_bounds(enabled=True,
                                         potency_ratio=None,
                                         delimit_by_numbers=True,
                                           max_followers=7500,
                                           max_following=None,
                                           min_followers=10,
                                           min_following=50)

        session.set_do_follow(enabled=True, percentage=90, times=3)

        session.set_sleep_reduce(200)

        """ Get the list of non-followers
            I duplicated unfollow_users() to see a list of non-followers which I run once in a while when I time
            to review the list
        """
        # session.just_get_nonfollowers()

        # my account is small at the moment, so I keep smaller upper threshold
        session.set_quota_supervisor(enabled=True, # limit hourly and daily peak
                sleep_after=["likes", "comments", "follows", "unfollows", "server_calls"],
                sleepyhead=True, stochastic_flow=True, notify_me=True,
                peak_likes_hourly=24,
                peak_likes_daily=560,
                peak_comments_hourly=6,
                peak_comments_daily=120,
                peak_follows_hourly=6,
                peak_follows_daily=120,
                peak_unfollows_hourly=6,
                peak_unfollows_daily=120,
                peak_server_calls_hourly=120,
                peak_server_calls_daily=2600)
        """ End of parameter setting """


        """ Actions start here """
        # Unfollow users
        """ Users who were followed by InstaPy, but not have followed back will be removed in
            One week (168 * 60 * 60)
            Yes, I give a liberal one week time to follow [back] :)
        """
        session.unfollow_users(amount=100, instapy_followed_enabled=True, instapy_followed_param="all", style="RANDOM",
                               unfollow_after=168 * 60 * 60,
                               sleep_delay=600)

        # Like by tags
        """ I mostly use like by tags. I used to use a small list of targeted tags with a big 'amount' like 300
            But that resulted in lots of "insufficient links" messages. So I started using a huge list of tags with
            'amount' set to something small like 50. Probably this is not the best way to deal with "insufficient links"
            message. But I feel it is a quick work around.
        """

        # Like by hashtags
        hashtags = []
        # Read hashtags from file and shuffle
        with open('/home/telegram-instapy/resources/hashtags/tech.txt') as f:
            for line in f:
                hashtags.append(line.strip("\n").replace('#',''))
        random.shuffle(hashtags)
        session.like_by_tags(hashtags, amount=15)

        session.end()
    except:
        print(traceback.format_exc())

# DOCTOR INSTABOT
def doctorInstabotNew(InstaPy, username, password, proxy):
    try:
        session = InstaPy(username=username, password=password, headless_browser=True)
        session.login()

        """ Start of parameter setting """
        # don't like if a post already has more than 150 likes
        session.set_delimit_liking(enabled=True, max_likes=2000, min_likes=10)

        # set like delay to 40 seconds
        session.set_action_delays(enabled=True, like=5.2, randomize=True, random_range_from=40, random_range_to=100)

        """I used to have potency_ratio=-0.85 and max_followers=1200 for set_relationship_bounds()
            Having a stricter relationship bound to target only low profiles users was not very useful,
            as interactions/sever calls ratio was very low. I would reach the server call threshold for
            the day before even crossing half of the presumed safe limits for likes, follow and comments (yes,
            looks like quiet a lot of big(bot) managed accounts out there!!).
            So I relaxed it a bit to -0.50 and 2000 respectively.
        """
        session.set_relationship_bounds(enabled=True,
                                         potency_ratio=None,
                                         delimit_by_numbers=True,
                                           max_followers=5000,
                                           max_following=None,
                                           min_followers=10,
                                           min_following=200)

        session.set_do_follow(enabled=True, percentage=90, times=3)

        session.set_sleep_reduce(200)

        """ Get the list of non-followers
            I duplicated unfollow_users() to see a list of non-followers which I run once in a while when I time
            to review the list
        """
        # session.just_get_nonfollowers()

        # my account is small at the moment, so I keep smaller upper threshold
        session.set_quota_supervisor(enabled=True,
                                      sleep_after=["likes", "comments_d", "follows", "unfollows", "server_calls_h"],
                                      sleepyhead=True, stochastic_flow=True, notify_me=True,
                                      peak_likes_hourly=57,
                                      peak_likes_daily=585,
                                      peak_comments_hourly=21,
                                      peak_comments_daily=182,
                                      peak_follows_hourly=48,
                                      peak_follows_daily=500,
                                      peak_unfollows_hourly=35,
                                      peak_unfollows_daily=402,
                                      peak_server_calls_hourly=None,
                                      peak_server_calls_daily=4700)
        """ End of parameter setting """


        """ Actions start here """
        # Unfollow users
        """ Users who were followed by InstaPy, but not have followed back will be removed in
            One week (168 * 60 * 60)
            Yes, I give a liberal one week time to follow [back] :)
        """
        session.unfollow_users(amount=25, instapy_followed_enabled=True, instapy_followed_param="all", style="RANDOM",
                               unfollow_after=168 * 60 * 60,
                               sleep_delay=600)

        # Like by tags
        """ I mostly use like by tags. I used to use a small list of targeted tags with a big 'amount' like 300
            But that resulted in lots of "insufficient links" messages. So I started using a huge list of tags with
            'amount' set to something small like 50. Probably this is not the best way to deal with "insufficient links"
            message. But I feel it is a quick work around.
        """

        # Like by hashtags
        hashtags = []
        # Read hashtags from file and shuffle
        with open('/home/telegram-instapy/resources/hashtags/doctor.txt') as f:
            for line in f:
                hashtags.append(line.strip("\n").replace('#',''))
        random.shuffle(hashtags)
        session.like_by_tags(hashtags, amount=35)

        session.end()
    except:
        print(traceback.format_exc())

# SKALIDIS INSTABOT
def skalidisInstabotNew(InstaPy, username, password, proxy):
    try:
        session = InstaPy(username=username, password=password, headless_browser=True)
        session.login()

        """ Start of parameter setting """
        # don't like if a post already has more than 150 likes
        session.set_delimit_liking(enabled=True, max_likes=1000, min_likes=10)

        # set like delay to 40 seconds
        session.set_action_delays(enabled=True, like=5.2, randomize=True, random_range_from=40, random_range_to=100)

        """I used to have potency_ratio=-0.85 and max_followers=1200 for set_relationship_bounds()
            Having a stricter relationship bound to target only low profiles users was not very useful,
            as interactions/sever calls ratio was very low. I would reach the server call threshold for
            the day before even crossing half of the presumed safe limits for likes, follow and comments (yes,
            looks like quiet a lot of big(bot) managed accounts out there!!).
            So I relaxed it a bit to -0.50 and 2000 respectively.
        """
        session.set_relationship_bounds(enabled=True,
                                         potency_ratio=None,
                                         delimit_by_numbers=True,
                                           max_followers=5000,
                                           max_following=None,
                                           min_followers=10,
                                           min_following=200)

        session.set_do_follow(enabled=True, percentage=90, times=3)

        session.set_sleep_reduce(200)

        """ Get the list of non-followers
            I duplicated unfollow_users() to see a list of non-followers which I run once in a while when I time
            to review the list
        """
        # session.just_get_nonfollowers()

        # my account is small at the moment, so I keep smaller upper threshold
        session.set_quota_supervisor(enabled=True,
                                      sleep_after=["likes", "comments_d", "follows", "unfollows", "server_calls_h"],
                                      sleepyhead=True, stochastic_flow=True, notify_me=True,
                                      peak_likes_hourly=57,
                                      peak_likes_daily=585,
                                      peak_comments_hourly=21,
                                      peak_comments_daily=182,
                                      peak_follows_hourly=48,
                                      peak_follows_daily=500,
                                      peak_unfollows_hourly=35,
                                      peak_unfollows_daily=402,
                                      peak_server_calls_hourly=None,
                                      peak_server_calls_daily=4700)
        """ End of parameter setting """


        """ Actions start here """
        # Unfollow users
        """ Users who were followed by InstaPy, but not have followed back will be removed in
            One week (168 * 60 * 60)
            Yes, I give a liberal one week time to follow [back] :)
        """
        session.unfollow_users(amount=25, instapy_followed_enabled=True, instapy_followed_param="all", style="RANDOM",
                               unfollow_after=168 * 60 * 60,
                               sleep_delay=600)

        # Like by tags
        """ I mostly use like by tags. I used to use a small list of targeted tags with a big 'amount' like 300
            But that resulted in lots of "insufficient links" messages. So I started using a huge list of tags with
            'amount' set to something small like 50. Probably this is not the best way to deal with "insufficient links"
            message. But I feel it is a quick work around.
        """

        # Like by hashtags
        hashtags = []
        # Read hashtags from file and shuffle
        with open('/home/telegram-instapy/resources/hashtags/doctor.txt') as f:
            for line in f:
                hashtags.append(line.strip("\n").replace('#',''))
        random.shuffle(hashtags)
        session.like_by_tags(hashtags, amount=15)

        session.end()
    except:
        print(traceback.format_exc())

# PHOTOGRAPHY INSTABOT
def photoInstabotNew(InstaPy, username, password, proxy):
    try:
        session = InstaPy(username=username, password=password, headless_browser=True)
        session.login()

        """ Start of parameter setting """
        # don't like if a post already has more than 150 likes
        session.set_delimit_liking(enabled=True, max_likes=1000, min_likes=10)

        # set like delay to 40 seconds
        session.set_action_delays(enabled=True, like=5.2, randomize=True, random_range_from=40, random_range_to=100)

        """I used to have potency_ratio=-0.85 and max_followers=1200 for set_relationship_bounds()
            Having a stricter relationship bound to target only low profiles users was not very useful,
            as interactions/sever calls ratio was very low. I would reach the server call threshold for
            the day before even crossing half of the presumed safe limits for likes, follow and comments (yes,
            looks like quiet a lot of big(bot) managed accounts out there!!).
            So I relaxed it a bit to -0.50 and 2000 respectively.
        """
        session.set_relationship_bounds(enabled=True,
                                         potency_ratio=None,
                                         delimit_by_numbers=True,
                                           max_followers=7500,
                                           max_following=None,
                                           min_followers=10,
                                           min_following=200)

        session.set_do_follow(enabled=True, percentage=90, times=3)

        session.set_sleep_reduce(200)

        """ Get the list of non-followers
            I duplicated unfollow_users() to see a list of non-followers which I run once in a while when I time
            to review the list
        """
        # session.just_get_nonfollowers()

        # my account is small at the moment, so I keep smaller upper threshold
        session.set_quota_supervisor(enabled=True,
                                      sleep_after=["likes", "comments_d", "follows", "unfollows", "server_calls_h"],
                                      sleepyhead=True, stochastic_flow=True, notify_me=True,
                                      peak_likes_hourly=57,
                                      peak_likes_daily=585,
                                      peak_comments_hourly=21,
                                      peak_comments_daily=182,
                                      peak_follows_hourly=48,
                                      peak_follows_daily=500,
                                      peak_unfollows_hourly=35,
                                      peak_unfollows_daily=402,
                                      peak_server_calls_hourly=None,
                                      peak_server_calls_daily=4700)
        """ End of parameter setting """


        """ Actions start here """
        # Unfollow users
        """ Users who were followed by InstaPy, but not have followed back will be removed in
            One week (168 * 60 * 60)
            Yes, I give a liberal one week time to follow [back] :)
        """
        session.unfollow_users(amount=50, instapy_followed_enabled=True, instapy_followed_param="all", style="RANDOM",
                               unfollow_after=168 * 60 * 60,
                               sleep_delay=600)

        # Like by tags
        """ I mostly use like by tags. I used to use a small list of targeted tags with a big 'amount' like 300
            But that resulted in lots of "insufficient links" messages. So I started using a huge list of tags with
            'amount' set to something small like 50. Probably this is not the best way to deal with "insufficient links"
            message. But I feel it is a quick work around.
        """

        # Like by hashtags
        hashtags = []
        # Read hashtags from file and shuffle
        with open('/home/telegram-instapy/resources/hashtags/photography.txt') as f:
            for line in f:
                hashtags.append(line.strip("\n").replace('#',''))
        random.shuffle(hashtags)
        session.like_by_tags(hashtags, amount=15)

        session.end()
    except:
        print(traceback.format_exc())


# MYKONOS INSTABOT
def mykonosInstabotNew(InstaPy, username, password, proxy):
    try:
        session = InstaPy(username=username, password=password, headless_browser=True)
        session.login()

        """ Start of parameter setting """
        # don't like if a post already has more than 150 likes
        session.set_delimit_liking(enabled=True, max_likes=500, min_likes=10)

        # set like delay to 40 seconds
        session.set_action_delays(enabled=True, like=5.2, randomize=True, random_range_from=40, random_range_to=100)

        """I used to have potency_ratio=-0.85 and max_followers=1200 for set_relationship_bounds()
            Having a stricter relationship bound to target only low profiles users was not very useful,
            as interactions/sever calls ratio was very low. I would reach the server call threshold for
            the day before even crossing half of the presumed safe limits for likes, follow and comments (yes,
            looks like quiet a lot of big(bot) managed accounts out there!!).
            So I relaxed it a bit to -0.50 and 2000 respectively.
        """
        session.set_relationship_bounds(enabled=True,
                                         potency_ratio=None,
                                         delimit_by_numbers=True,
                                           max_followers=5000,
                                           max_following=None,
                                           min_followers=10,
                                           min_following=200)

        session.set_do_follow(enabled=True, percentage=90, times=3)

        session.set_sleep_reduce(200)

        """ Get the list of non-followers
            I duplicated unfollow_users() to see a list of non-followers which I run once in a while when I time
            to review the list
        """
        # session.just_get_nonfollowers()

        # my account is small at the moment, so I keep smaller upper threshold
        session.set_quota_supervisor(enabled=True,
                                      sleep_after=["likes", "comments_d", "follows", "unfollows", "server_calls_h"],
                                      sleepyhead=True, stochastic_flow=True, notify_me=True,
                                      peak_likes_hourly=57,
                                      peak_likes_daily=585,
                                      peak_comments_hourly=21,
                                      peak_comments_daily=182,
                                      peak_follows_hourly=48,
                                      peak_follows_daily=500,
                                      peak_unfollows_hourly=35,
                                      peak_unfollows_daily=402,
                                      peak_server_calls_hourly=None,
                                      peak_server_calls_daily=4700)
        """ End of parameter setting """


        """ Actions start here """
        # Unfollow users
        """ Users who were followed by InstaPy, but not have followed back will be removed in
            One week (168 * 60 * 60)
            Yes, I give a liberal one week time to follow [back] :)
        """
        session.unfollow_users(amount=25, instapy_followed_enabled=True, instapy_followed_param="all", style="RANDOM",
                               unfollow_after=168 * 60 * 60,
                               sleep_delay=600)

        # Like by tags
        """ I mostly use like by tags. I used to use a small list of targeted tags with a big 'amount' like 300
            But that resulted in lots of "insufficient links" messages. So I started using a huge list of tags with
            'amount' set to something small like 50. Probably this is not the best way to deal with "insufficient links"
            message. But I feel it is a quick work around.
        """

        # Like by hashtags
        hashtags = []
        # Read hashtags from file and shuffle
        with open('/home/telegram-instapy/resources/hashtags/mykonos.txt') as f:
            for line in f:
                hashtags.append(line.strip("\n").replace('#',''))
        random.shuffle(hashtags)
        session.like_by_tags(hashtags, amount=15)

        session.end()
    except:
        print(traceback.format_exc())

# !! Not delete the following code.
class Scripts:
    def __init__(self):
        functions = [f for fname, f in sorted(globals().items()) if callable(f)]
        self.scripts = {}
        for function in functions:
            name = str(function.__name__).lower()
            if name != "scripts":
                self.scripts[name] = function
